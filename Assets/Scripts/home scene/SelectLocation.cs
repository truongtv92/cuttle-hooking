using System.Collections;
using Common;
using UnityEngine;
using UnityEngine.UI;

namespace home_scene
{
    public class SelectLocation : MonoBehaviour
    {
        [SerializeField] private float duration = 0.5f;
        [SerializeField] private ScrollRect scrollRect;
        private int numberLocation = 3;
        private int currentLocation;
        private void Start()
        {
            currentLocation = 0;
        }
      
        public void OnNextButtonClick()
        {
            currentLocation++;
            if(currentLocation>2) {
                currentLocation = 2;
                return;
            }
            SnapTo(scrollRect.content.GetChild(currentLocation).GetComponent<RectTransform>());
           // StartCoroutine(MoveTo(currentCharacter));

        }

        public void OnPrevButtonClick()
        {
            currentLocation--;
            if(currentLocation<0) {
                currentLocation = 0;
                return;
            }
            SnapTo(scrollRect.content.GetChild(currentLocation).GetComponent<RectTransform>());
           // StartCoroutine(MoveTo(currentCharacter));
        }

        public void OnPlayButtonClick()
        {
            Config.currentMap = currentLocation;
            ScreenManager.Instance.LoadSceneNormal("Map"+(Config.currentMap+1), ()=> { GamePlay.Instance.StartCountDown(); });
        }

        private void SnapTo(Transform target)
        {
            Canvas.ForceUpdateCanvases();
            var from = scrollRect.content.anchoredPosition;
            var to = (Vector2)scrollRect.transform.InverseTransformPoint(scrollRect.content.position)
                     - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
            StartCoroutine(UpdatePosition(from, to));
        }
        private IEnumerator UpdatePosition(Vector2 from, Vector2 to)
        {
            for (float timeCount = 0; timeCount < duration; timeCount += Time.deltaTime)
            {
                var progress = timeCount / duration;
                scrollRect.content.anchoredPosition = new Vector2(Mathf.Lerp(from.x, to.x, progress),Mathf.Lerp(from.y, to.y, progress));
                yield return null;
            }

            scrollRect.content.anchoredPosition = to;
            yield return null;
        }
    }
}