using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class OpenHyperlinks : MonoBehaviour,IPointerClickHandler
{
    public TMP_Text pTextMeshPro;
    public Camera pCamera;
    public void OnPointerClick(PointerEventData eventData)
    {
        var linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, Input.mousePosition, pCamera);
        if (linkIndex == -1) return; // was a link clicked?
        var linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];

        // open the link id as a url, which is the metadata we added in the text field
        Application.OpenURL(linkInfo.GetLinkID());
    }
}