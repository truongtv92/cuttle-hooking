using DG.Tweening;
using TMPro;
using UnityEngine;

namespace home_scene
{
    public class HighScore : MonoBehaviour
    {
        public GameObject top1,top2,top3;
        public TextMeshProUGUI TxtTop1Time, TxtTop1Value, TxtTop2Time, TxtTop2Value, TxtTop3Time, TxtTop3Value;

        private void Awake()
        {
            
            if (PlayerPrefs.GetFloat("3rd_value") == 0)
            {
                top3.SetActive(false);
            }
            else
            {
                TxtTop3Time.text = PlayerPrefs.GetString("3rd_time");
                TxtTop3Value.text = PlayerPrefs.GetFloat("3rd_value").ToString("0.00") +" kg";
            }

            if (PlayerPrefs.GetFloat("2nd_value") == 0)
            {
                top2.SetActive(false);
            }
            else
            {
                TxtTop2Time.text = PlayerPrefs.GetString("2nd_time");
                TxtTop2Value.text = PlayerPrefs.GetFloat("2nd_value").ToString("0.00") +" kg";
            }

            if (PlayerPrefs.GetFloat("1st_value") == 0)
            {
                top1.SetActive(false);
            }
            else
            {
                TxtTop1Time.text = PlayerPrefs.GetString("1st_time");
                TxtTop1Value.text = PlayerPrefs.GetFloat("1st_value").ToString("0.00")+" kg";
            }

            GetComponent<DOTweenAnimation>().DORewind();
            GetComponent<DOTweenAnimation>().DOPlay();
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}