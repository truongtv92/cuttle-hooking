using home_scene;
using UnityEngine;

public class Link : MonoBehaviour
{
    public string link;
    public OpenLink openlink;

    public void OnClick()
    {
       // Application.OpenURL(link);
       openlink.gameObject.SetActive(true);
       openlink.Init(link);
    }
}