using System.Collections;
using Common;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace home_scene
{
    public class CharacterSelector : MonoBehaviour
    {
        [SerializeField] private float duration = 0.5f;
        [SerializeField] private ScrollRect scrollRect;
        private int numberCharacter = 3;
        private int currentCharacter;
        public GameObject mapSelector;
        private void Start()
        {
            currentCharacter = 0;
        }
      
        public void OnNextButtonClick()
        {
            currentCharacter++;
            if(currentCharacter>2) {
                currentCharacter = 2;
                return;
            }
            SnapTo(scrollRect.content.GetChild(currentCharacter).GetComponent<RectTransform>());
           // StartCoroutine(MoveTo(currentCharacter));

        }

        public void OnPrevButtonClick()
        {
            currentCharacter--;
            if(currentCharacter<0) {
                currentCharacter = 0;
                return;
            }
            SnapTo(scrollRect.content.GetChild(currentCharacter).GetComponent<RectTransform>());
           // StartCoroutine(MoveTo(currentCharacter));
        }

        public void OnPlayButtonClick()
        {
            Config.characterIndex = currentCharacter;
            mapSelector.SetActive(true);
            mapSelector.GetComponent<DOTweenAnimation>().DORewind();
            mapSelector.GetComponent<DOTweenAnimation>().DOPlay();
            gameObject.SetActive(false);
        }

        private void SnapTo(Transform target)
        {
            Canvas.ForceUpdateCanvases();
            var from = scrollRect.content.anchoredPosition;
            var to = (Vector2)scrollRect.transform.InverseTransformPoint(scrollRect.content.position)
                     - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
            StartCoroutine(UpdatePosition(from, to));
        }
        private IEnumerator UpdatePosition(Vector2 from, Vector2 to)
        {
            for (float timeCount = 0; timeCount < duration; timeCount += Time.deltaTime)
            {
                var progress = timeCount / duration;
                scrollRect.content.anchoredPosition = new Vector2(Mathf.Lerp(from.x, to.x, progress),Mathf.Lerp(from.y, to.y, progress));
                yield return null;
            }

            scrollRect.content.anchoredPosition = to;
            yield return null;
        }
    }
}