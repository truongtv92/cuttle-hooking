﻿using Common;
using DG.Tweening;
using UnityEngine;

public class MenuScene : MonoBehaviour
{
   public GameObject main;
   public GameObject credit;
   public GameObject inputNameObj;
   public GameObject chooseCharacter;
   public GameObject highScore;
   public GameObject rewardViewer;
   public GameObject chooseMap;
   private void Awake()
   {
      inputNameObj.SetActive(PlayerPrefs.GetString("display_name").Trim().Equals(string.Empty));
      SoundManager.Instance.PlayMenuBackgroundMusic();
      
   }

   public void OnPlayButtonClick()
   {
      SoundManager.Instance.playButtonClick();
      main.SetActive(false);
      chooseCharacter.transform.parent.gameObject.SetActive(true);
      chooseCharacter.SetActive(true);
      chooseCharacter.GetComponent<DOTweenAnimation>().DORewind();
      chooseCharacter.GetComponent<DOTweenAnimation>().DOPlay();
   }

   public void OnCreditButtonClick()
   {  
      highScore.SetActive(false);
      credit.SetActive(true);
      credit.GetComponent<DOTweenAnimation>().DORewind();
      credit.GetComponent<DOTweenAnimation>().DOPlay();
   }

   public void OnRewardButtonClick()
   {
      rewardViewer.SetActive(true);
      rewardViewer.GetComponentInChildren<DOTweenAnimation>().DORewind();
      rewardViewer.GetComponentInChildren<DOTweenAnimation>().DOPlay();
   }
   public void CloseCredit()
   {
      credit.SetActive(false);
   }

   public void CloseSelectCharacterPopup()
   {
      main.SetActive(true);
      chooseCharacter.SetActive(false);
      
   }

   public void showHighScore()
   {
      highScore.SetActive(true);
      credit.SetActive(false);
      highScore.GetComponent<DOTweenAnimation>().DORewind();
      highScore.GetComponent<DOTweenAnimation>().DOPlay();
   }
}
