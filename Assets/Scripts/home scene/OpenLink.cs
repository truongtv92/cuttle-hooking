using DG.Tweening;
using TMPro;
using UnityEngine;

namespace home_scene
{
    public class OpenLink : MonoBehaviour
    {
        public TextMeshProUGUI Txt;
        public string link;

        public void Init(string linkInit)
        {
            link = linkInit;
            Txt.text = "Do you want to visit\n" + link;
        }

        public void Open()
        {
            Application.OpenURL(link);
            gameObject.SetActive(false);
        }

    }
}