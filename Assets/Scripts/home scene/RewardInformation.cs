using System.Collections;
using Common;
using UnityEngine;
using UnityEngine.UI;

namespace home_scene
{
    public class RewardInformation : MonoBehaviour
    {
        [SerializeField] private float duration = 0.5f;
        [SerializeField] private ScrollRect scrollRect;
        private int numberReward = 7;
        private int currentReward;
        private void Start()
        {
            currentReward = 0;
        }
      
        public void OnNextButtonClick()
        {
            currentReward++;
            if(currentReward>6) {
                currentReward = 6;
                return;
            }
            SnapTo(scrollRect.content.GetChild(currentReward).GetComponent<RectTransform>());
           // StartCoroutine(MoveTo(currentCharacter));

        }

        public void OnPrevButtonClick()
        {
            currentReward--;
            if(currentReward<0) {
                currentReward = 0;
                return;
            }
            SnapTo(scrollRect.content.GetChild(currentReward).GetComponent<RectTransform>());
           // StartCoroutine(MoveTo(currentCharacter));
        }
        private void SnapTo(Transform target)
        {
            Canvas.ForceUpdateCanvases();
            var from = scrollRect.content.anchoredPosition;
            var to = (Vector2)scrollRect.transform.InverseTransformPoint(scrollRect.content.position)
                     - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
            StartCoroutine(UpdatePosition(from, to));
        }
        private IEnumerator UpdatePosition(Vector2 from, Vector2 to)
        {
            for (float timeCount = 0; timeCount < duration; timeCount += Time.deltaTime)
            {
                var progress = timeCount / duration;
                scrollRect.content.anchoredPosition = new Vector2(Mathf.Lerp(from.x, to.x, progress),Mathf.Lerp(from.y, to.y, progress));
                yield return null;
            }

            scrollRect.content.anchoredPosition = to;
            yield return null;
        }
    }
}