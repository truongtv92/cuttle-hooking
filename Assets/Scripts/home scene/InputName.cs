using System;
using TMPro;
using UnityEngine;

namespace home_scene
{
    public class InputName : MonoBehaviour
    {
        public TMP_InputField inputname;
        public TextMeshProUGUI txtError;

        public void OnConfirmButtonClick()
        {
            if (inputname.text.Trim().Equals(string.Empty))
            {
                txtError.text = "Error: Display Name cant be empty!";
                return;
            }

            if (inputname.text.Trim().Length < 3)
            {
                txtError.text = "Error: Display Name's length must be from 3 to 16 letters";
                return;
            }
            PlayerPrefs.SetString("display_name",inputname.text.Trim());
            gameObject.SetActive(false);
        }
    }
}