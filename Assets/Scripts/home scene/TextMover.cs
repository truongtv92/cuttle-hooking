using UnityEngine;

public class TextMover : MonoBehaviour
{
       public float top,bottom,speed;

       private void OnEnable()
       {
              GetComponent<RectTransform>().localPosition = new Vector3(0,bottom,0);
       }

       private void Update()
       {
              if (GetComponent<RectTransform>().localPosition.y > top)
              {
                     GetComponent<RectTransform>().localPosition = new Vector3(0,bottom,0);
              }
              GetComponent<RectTransform>().localPosition+= new Vector3(0,speed*Time.deltaTime,0);
       }
}