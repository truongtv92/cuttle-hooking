using UnityEngine;

namespace Common
{
    public class SoundManager : Singleton<SoundManager>
    {
        public AudioClip soundMain, soundPlay;
        public AudioClip buttonsound;
        public AudioClip hookSound;
        public AudioSource child;
        public AudioClip[] endSounds;
        public AudioClip[] menAngry;
        public AudioClip[] menHappy;
        public AudioClip[] womenAngry;
        public AudioClip[] womenHappy;
        public AudioClip[] childAngry;
        public AudioClip[] childHappy;

        // public AudioClip 
        public void PlayInGameBackgroundMusic()
        {
            GetComponent<AudioSource>().clip = soundPlay;
            GetComponent<AudioSource>().Play();
        }

        public void StopBackgroundMusic()
        {
            GetComponent<AudioSource>().Stop();
        }
        public void PlayMenuBackgroundMusic()
        {
            GetComponent<AudioSource>().clip = soundMain;
            GetComponent<AudioSource>().Play();
        }
        public void PlayEndSound()
        {
            StopBackgroundMusic();
            child.clip = endSounds[0];
            child.Play();
        }

        public void playButtonClick()
        {
            child.clip = buttonsound;
            child.Play();
        }

        public void PlayMissSound()
        {
            if (Config.characterIndex == 0)//child
            {
                child.clip = childAngry[Random.Range(0,childAngry.Length)];
                child.Play();
            }
            else if (Config.characterIndex == 1) //man
            {
                child.clip = menAngry[Random.Range(0,menAngry.Length)];
                child.Play();
            }
            else
            {
                child.clip = womenAngry[Random.Range(0,womenAngry.Length)];
                child.Play();
            }
        }

        public void PlayHookrightSound()
        {
            if (Config.characterIndex == 0)//child
            {
                child.clip = childHappy[Random.Range(0,childHappy.Length)];
                child.Play();
            }
            else if (Config.characterIndex == 1) //man
            {
                child.clip = menHappy[Random.Range(0,menHappy.Length)];
                child.Play();
            }
            else
            {
                child.clip = womenHappy[Random.Range(0,womenHappy.Length)];
                child.Play();
            }
        }

        public void PlaySquidHookRightSound()
        {
            child.clip = hookSound;
            child.Play();
        }
    }
}