﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager:Singleton<ScreenManager>
{
    [SerializeField]
    private LockScene lockScene;
    public void LoadSceneNormal(string newScene,Action completeAction =null){
        StartCoroutine(StartLoadSceneNormal(newScene,completeAction));
    }



    #region Private Method

    private IEnumerator StartLoadSceneNormal(string nameScene,Action completeAction=null)
    {
        lockScene.SetActive(true);
        yield return new WaitForEndOfFrame();
        var sceneCustoms = FindObjectsOfType<SceneCustom>();
        foreach (var sceneCustom in sceneCustoms)
        {
            sceneCustom.FadeOut();
        }
        AsyncOperation loadingOperation = null;
        var  result = false;
        while (result == false)
        {
            result = sceneCustoms.Aggregate(true, (current, sceneCustom) => current && sceneCustom.IsFadeOutDone);
            yield return null;
        }
        yield return null;
        LoadSceneSingle(nameScene, asyncOperation =>
        {
            loadingOperation = asyncOperation;
            loadingOperation.allowSceneActivation = false;
        });
        while (loadingOperation==null)
        {
            yield return null;
        }
        loadingOperation.allowSceneActivation = true;
        SceneCustom.LastScene.FadeIn();
        while (!SceneCustom.LastScene.IsFadeInDone)
        {
            yield return null;
        }
        yield return null;
        lockScene.SetActive(false);
        completeAction?.Invoke();
    }

    private void LoadSceneSingle(string nameScene, Action<AsyncOperation> completeAction = null)
    {
        StartCoroutine(LoadLevel(nameScene, LoadSceneMode.Single, completeAction));
    }
    private static IEnumerator LoadLevel(string name, LoadSceneMode loadSceneMode,
        Action<AsyncOperation> completeAction = null)
    {
        yield return new WaitForEndOfFrame();
        var asyncOperation = SceneManager.LoadSceneAsync(name, loadSceneMode);
        if (completeAction != null)
            asyncOperation.completed += completeAction;
        yield return asyncOperation;
        while (!asyncOperation.isDone)
        {
            yield return null;
        }

        if (completeAction == null)
            asyncOperation.allowSceneActivation = true;
        else completeAction.Invoke(asyncOperation);
    }



    #endregion
    
}