using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common
{
    public class BaseButton : EventTrigger
    {
        public bool isFinish = false;
       // public AnimationCurve ReleaseAniamtionCurve;
       private Sequence _sequence;
        public override void OnPointerDown(PointerEventData data)
        {
            var scaleX = transform.localScale.x < 0 ? -1 : 1;
            var scaleY = transform.localScale.y < 0 ? -1 : 1;
            isFinish = false;
             _sequence = DOTween.Sequence();
             _sequence.Append(transform.DOScale(new Vector3(scaleX*0.9f,scaleY*0.9f,0.9f), 0.2f));
             _sequence.SetEase(Ease.OutQuad);
             _sequence.SetRelative(false);
             _sequence.OnComplete (delegate
             {
                 isFinish = true;
                 transform.localScale = new Vector3(scaleX,scaleY,1);
                _sequence.Kill ();
            });
             _sequence.Play();
        }

        public override void OnPointerUp(PointerEventData data)
        {
            StartCoroutine(UpEvent());

        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            SoundManager.Instance.playButtonClick();
        }

        IEnumerator UpEvent()
        {
            
            while (!isFinish)
            {
                yield return null;
            }
            var sequence = DOTween.Sequence();
            var scaleX = transform.localScale.x < 0 ? -1 : 1;
            var scaleY = transform.localScale.y < 0 ? -1 : 1;
            sequence.Append(transform.DOPunchScale(new Vector3(scaleX*0.1f,scaleY*0.1f,0.1f), 0.2f,3,0.5f));
            sequence.SetEase(Ease.OutQuad);
            sequence.SetRelative(false);
            sequence.OnComplete (delegate
            {
                sequence.Kill ();
            });
            sequence.Play();
        }
        
    }
    
}