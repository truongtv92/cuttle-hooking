﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component
{
	private static T _instance;
	public static T Instance
	{
		get {
			if (_instance != null) return _instance;
			_instance = (T) FindObjectOfType (typeof (T));
			if (_instance == null)
			{
				Debug.LogError (typeof (T).Name + "not found");
			}
			return _instance;
		}
	}

	protected virtual void Awake ()
	{
		if (!CheckInstance ())
			Destroy (this);
			
	}

	private bool CheckInstance ()
	{
		return this == Instance;
	}

}