namespace Common
{
    public static class Config
    {
        public static int characterIndex = 0;
        public const float REWARD_EASY = 20f;
        public const float REWARD_NORMAL = 30f;
        public const float REWARD_HARD = 47.5f;
        private const float BASIC_VALUE = 50f;
        public static int currentMap = 0;

        public static float CalculateProcess(float point)
        {
//            if (point >= 20) return 1;
//            if (point > 19 && point<20) return 0.9f + (point - 19) * 0.1f;
//            if (point <= 19 && point > 18) return 0.6f + (point - 18) * 0.3f;
//            if (point <= 18 && point > 16) return 0.25f + (point - 16) * 0.2f;
//            if (point <= 16f) return point*0.0125f;
//            return 0;
            return point / BASIC_VALUE;
        }
    }
}