using System.Collections;
using UnityEngine;

public class SceneCustom : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private float timeFadeIn = 1f;
    [SerializeField] private float timeFadeOut = 1f;
    public bool IsFadeInDone { get;private set; }
    public bool IsFadeOutDone { get; private set; }
    public static SceneCustom LastScene { get; private set; }

    #region Method

    private void Awake()
    {
        LastScene = this;
    }

    #endregion
    public void FadeIn()
    {
        canvasGroup.alpha = 0;
        IsFadeInDone = false;
        StartCoroutine(FadeInCanvas());
    }

    public void FadeOut()
    {
        canvasGroup.alpha = 1;
        IsFadeOutDone = false;
        StartCoroutine(FadeOutCanvas());
    }
    private IEnumerator FadeInCanvas( float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime/timeFadeIn;
            yield return null;
        }
        IsFadeInDone = true;
        yield return null;
    }
    private IEnumerator FadeOutCanvas(float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime /timeFadeOut;
            yield return null;
        }

        yield return null;
        IsFadeOutDone = true;
    }
}