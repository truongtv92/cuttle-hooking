using UnityEngine;

public class FishingLine : Singleton<FishingLine>
{
    [SerializeField]
    private Transform fishingHook;
    public Vector3 angles;
    public float speed = 5;
    public float angleMax = 70;
    public TypeAction typeAction = TypeAction.Idle;
   
    // Update is called once per frame
    private void Update () {
        if (GamePlay.Instance.gameState != GameState.Play && GamePlay.Instance.gameState != GameState.CountDown) return;
        gameObject.GetComponent<LineRenderer>().SetPosition(0, transform.position);
        gameObject.GetComponent<LineRenderer>().SetPosition(1, fishingHook.position);

    }

    private void FixedUpdate() {
        if (GamePlay.Instance.gameState != GameState.Play && GamePlay.Instance.gameState != GameState.CountDown) return;
        if(speed > 0 && typeAction == TypeAction.Idle)
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Sin(Time.time * speed) * angleMax);
    }
//
//    void pendulum(float x, float y, float z) {
//        transform.rotation = Quaternion.Euler(0, 0, Mathf.Sin(Time.time * speed) * angleMax);
//    }
}
public enum TypeAction {Idle,Drop,Pull,Wait};