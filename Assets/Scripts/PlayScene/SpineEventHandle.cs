using Spine;
using Spine.Unity;
using UnityEngine;

namespace PlayScene
{
    public class SpineEventHandle : MonoBehaviour
    {
        public SkeletonAnimation skeletonAnimation;
        public ReceiveReward receive;

        private void Start()
        {
            skeletonAnimation.AnimationState.Event += HandleEvent;
        }

        private void HandleEvent (TrackEntry trackEntry, Spine.Event e) {
            // Play some sound if the event named "footstep" fired.
            switch (e.Data.Name)
            {
                case "EndDrop":
                    skeletonAnimation.loop = false;
                    skeletonAnimation.AnimationName = "Open";
                    break;
                case "EndOpen":
                    skeletonAnimation.loop = true;
                    skeletonAnimation.AnimationName = "Idle open";
                    receive.ShowPopUpReward();
                    
                    break;
                default:
                    break;
                    
            }
            
        }
    }
}