using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Slider timer;
    [SerializeField] private float duration;
    [SerializeField] private float percentDecrease = 0.05f;
    private bool _isAddMoreTime;

    public void AddMoreTime(float value)
    {
        _isAddMoreTime = true;
        var currentValue = timer.value;
        var nextValue = currentValue + value > 1 ? 1 : currentValue + value;
        StartCoroutine(UpdateTimer(currentValue, nextValue));
    }

    public void StartCountDown()
    {
        StartCoroutine(CountDown());
    }

    private IEnumerator UpdateTimer(float last, float current)
    {
        for (float timeCount = 0; timeCount < duration; timeCount += Time.deltaTime)
        {
            var progress = timeCount / duration;
            timer.value = Mathf.Lerp(last, current, progress);
            yield return null;
        }

        timer.value = current;
        _isAddMoreTime = false;
        yield return null;
    }

    private IEnumerator CountDown()
    {
        while (timer.value > 0)
        {
            if (!_isAddMoreTime)
            {
                timer.value -= percentDecrease*Time.deltaTime;
            }

            yield return new WaitForSeconds(Time.deltaTime);
        }

        TimeEnd();
    }

    private static void TimeEnd()
    {
        GamePlay.Instance.EndGame();
    }
}