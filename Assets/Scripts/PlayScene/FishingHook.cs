using UnityEngine;

public class FishingHook : Singleton<FishingHook>
{
    public float speed;

    public float speedBegin;
    public Vector2 velocity;
    public float maxY;
    public Transform target;

    private Vector3 _prePosition;
    private bool isHookFalse = false;
    public float timeWait = 0.4f;

//	public bool isUpSpeed;
//	public float timeUpSpeed;
    // Use this for initialization
    private void Start()
    {
        //isUpSpeed = false;
        _prePosition = transform.localPosition;

//		this.StartCoroutine("TimeUpSpeed");
    }

//	public IEnumerator TimeUpSpeed ()
//	{
//		while(true){
//			if(isUpSpeed)
//			{
//				timeUpSpeed = timeUpSpeed - 1;
//				if(timeUpSpeed <= 0)
//					isUpSpeed = false;
//			}
//			yield return new WaitForSeconds (1);
//		}

    private void Update()
    {
        if (GamePlay.Instance.gameState != GameState.Play) return;
        
        CheckPullFishingHookFinish();
        //TouchScene();
        CheckMoveOutCameraView();


    }

    private void FixedUpdate()
    {
        
        if (GamePlay.Instance.gameState == GameState.Pause || GamePlay.Instance.gameState == GameState.Finish)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
        else
        {
            if (FishingLine.Instance.typeAction == TypeAction.Drop ||
                FishingLine.Instance.typeAction == TypeAction.Pull)
                GetComponent<Rigidbody2D>().velocity = velocity * speed;
        }

        
    }

    private bool IsPositionOutBound()
    {
        var position = gameObject.transform.position;
        return Mathf.Abs(position.x)>=9.2f || position.y < -5.35f;
    }

    public void TouchScene()
    {
        if (GamePlay.Instance.gameState != GameState.Play) return;
        if ( FishingLine.Instance.typeAction != TypeAction.Idle) return;
        FishingLine.Instance.typeAction = TypeAction.Drop;
        var transform1 = transform;
        var position = target.position;
        var position1 = transform1.position;
        velocity = new Vector2(position1.x - position.x,
            position1.y - position.y);
        velocity.Normalize();

        speed = speedBegin;
    }

    //kiem tra khi luoi cau ra ngoai tam nhin cua camera
    private void CheckMoveOutCameraView()
    {
        if (FishingLine.Instance.typeAction != TypeAction.Drop) return;
        if (IsPositionOutBound() == false) return;
        FishingLine.Instance.typeAction = TypeAction.Pull;
        isHookFalse = true;
        velocity = -velocity;
    }

    //kiem tra khi luoi ca keo len mat nuoc
    private void CheckPullFishingHookFinish()
    {
        if (transform.localPosition.y < maxY || FishingLine.Instance.typeAction != TypeAction.Pull) return;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        FishingLine.Instance.typeAction = TypeAction.Wait;
        SetFishingHookToIdle();
        //Invoke(nameof(SetFishingHookToIdle),timeWait);
    }

    private void SetFishingHookToIdle()
    {
        FishingLine.Instance.typeAction = TypeAction.Idle;
        if (isHookFalse)
        {
            GamePlay.Instance.ShowEmotion(!isHookFalse);
        }
        else
        {
            
        }
        transform.localPosition = _prePosition;
    }
    public void StartPull(float speedGot)
    {
        velocity = -velocity;
        speed = speedGot;
    }
}