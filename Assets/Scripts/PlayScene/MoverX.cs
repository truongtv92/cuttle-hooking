using UnityEngine;

namespace DefaultNamespace
{
    public class MoverX : MonoBehaviour
    {
        public float left,right,speed;
        private void Update()
        {
            if (speed > 0)
            {
                if (GetComponent<RectTransform>().position.x > right)
                {
                    GetComponent<RectTransform>().position = new Vector3(left,GetComponent<RectTransform>().position.y,0);
                }
            }
            else
            {
                if (GetComponent<RectTransform>().position.x <left)
                {
                    GetComponent<RectTransform>().position = new Vector3(right,GetComponent<RectTransform>().position.y,0);
                }
            }
            
            GetComponent<RectTransform>().position+= new Vector3(speed*Time.deltaTime,0,0);
        }
    }
}