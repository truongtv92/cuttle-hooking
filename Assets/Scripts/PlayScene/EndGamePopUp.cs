using System.Collections;
using Common;
using UnityEngine;
using UnityEngine.UI;

namespace PlayScene
{
    public class EndGamePopUp : MonoBehaviour
    {
        public Slider processBar;
        public GameObject btnReTry, btnHome, btnTakeReward;
        public GameObject OpenReward;


        private const float duration = 0.5f;
        private float _totalScore;

        private void Awake()
        {
            GetComponent<Canvas>().worldCamera = Camera.main;
            SoundManager.Instance.PlayEndSound();
        }

        public void Init(float score)
        {
            _totalScore = score;
            
            //StartAnimation();
        }

        public void OnReTryButtonClick()
        {
            ScreenManager.Instance.LoadSceneNormal("Map"+(Config.currentMap+1), ()=> { GamePlay.Instance.StartCountDown(); });
        }

        public void OnHomeButtonClick()
        {
            ScreenManager.Instance.LoadSceneNormal("HomeScene");
        }

        public void OnTakeRewardButtonClick()
        {
            gameObject.SetActive(false);
            OpenReward.SetActive(true);
            OpenReward.GetComponent<ReceiveReward>().Init();
        }
        public void StartAnimation()
        {
            var current = Config.CalculateProcess(_totalScore);
            StartCoroutine(UpdateScore(current));
        }
        private IEnumerator UpdateScore( float current)
        {
            yield return new WaitForSeconds(0.2f);
            for (float timeCount = 0; timeCount < duration; timeCount += Time.deltaTime)
            {
                var progress = timeCount / duration;
                processBar.value= Mathf.Lerp(0, current, progress);
                //scoreText.text = "Score: "+data;
                yield return null;
            }

            processBar.value = current;
            // scoreText.text = "Score: "+current;
       
            yield return null;
            if (_totalScore < Config.REWARD_EASY)
            {
                btnReTry.SetActive(true);
                btnHome.SetActive(true);
            }
            else btnTakeReward.SetActive(true);
        }
    }
}