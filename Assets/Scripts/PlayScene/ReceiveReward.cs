using System;
using System.Collections;
using Common;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace PlayScene
{
    public class ReceiveReward : MonoBehaviour
    {
        private Reward _reward;
        public GameObject rewardPopUp;
        public TextMeshProUGUI TxtDescription;
        public Sprite[] rewardSprites;
        public Image icon;
        public Image light;
        public CanvasGroup itemReward;
        
        private void Awake()
        {
            GetComponent<Canvas>().worldCamera = Camera.main;
            TxtDescription.text =
                "congratulation Truongtv have won a smiling mutherfukin sun at "+DateTime.UtcNow+"GMT. Please take a screenshot to process the next step!";
        }

        public void Init()
        {
            var reward = GetReward(GamePlay.Instance.ScorePoint);
            var rewardName = GetRewardName(reward);
            TxtDescription.text =
                "Congratulations! "+PlayerPrefs.GetString("display_name")+" have won "+rewardName+" at "+DateTime.UtcNow+" GMT. Please take a screenshot to process your prize.";
            icon.sprite = GetRewardSprite(reward);
        }
        
        private Sprite GetRewardSprite(Reward reward)
        {
            switch (reward)
            {
                case Reward.Caps:
                    return rewardSprites[0];
                case Reward.Umbrella:
                    return rewardSprites[1];
                case Reward.Drinks:
                    return rewardSprites[2];
                case Reward.Booking_Hotel_Voucher:
                    return rewardSprites[4];
                case  Reward.Booking_Cruise_Voucher:
                    return  rewardSprites[3];
                case Reward.Booking_combo_Cruise_Hotel_voucher:
                    return rewardSprites[5];
                case  Reward.Paradise_Elegance_Free:
                    return rewardSprites[6];
                default:
                    return rewardSprites[0];
            }
        }
        private string GetRewardName(Reward reward)
        {
            switch (reward)
            {
                case Reward.Caps:
                    return "a Caps";
                case Reward.Umbrella:
                    return "an Umbrella";
                case Reward.Drinks:
                    return "a Free Drink";
                case Reward.Booking_Hotel_Voucher:
                    return "a Voucher 1.000.000VND when booking Paradise Suite Hotel";
                case  Reward.Booking_Cruise_Voucher:
                    return  "a Voucher 1.000.000VND when booking Paradise Cruise";
                case Reward.Booking_combo_Cruise_Hotel_voucher:
                    return  "a Voucher 1.000.000VND when booking combo Paradise Cruise and Hotel";
                case  Reward.Paradise_Elegance_Free:
                    return "a Voucher 2 Day- 1 Night in Paradise Elegance";
                default:
                    return "";
            }
        }

        private Reward GetReward(float point)
        {
            var type = 0;
            if (point >= Config.REWARD_HARD) type = 2;
            else if (point >= Config.REWARD_NORMAL) type = 1;
            else if (point >= Config.REWARD_EASY) type = 0;
            var reward = 0;
            switch (type)
            {
                case 0:
                    reward = Random.Range(0, 3);
                    break;
                case 1:
                    reward = Random.Range(3, 6);
                    break;
                case 2:
                    reward = 6;
                    break;
            }

            return (Reward) reward;
        }
        
        public void ShowPopUpReward()
        {
            Invoke(nameof(ShowPopUp),2f);
           // light.GetComponent<DOTweenAnimation>().DOPlay();
            itemReward.DOFade(1, 0.5f);
            // StartCoroutine(ShowItem());

        }

        IEnumerator ShowItem()
        {
            while (icon.color.a<1)
            {
                var tempcolor = icon.color;
                tempcolor.a  = tempcolor.a +Time.deltaTime;
                icon.color = tempcolor;
                    yield return null;
                    
            }
        }

        private void ShowPopUp()
        {
            rewardPopUp.SetActive(true);
        }
    }

    public enum Reward
    {
        Umbrella = 0,
        Caps = 1,
        Drinks = 2,
        Booking_Cruise_Voucher = 3,
        Booking_Hotel_Voucher = 4,
        Booking_combo_Cruise_Hotel_voucher = 5,
        Paradise_Elegance_Free = 6
    }
}