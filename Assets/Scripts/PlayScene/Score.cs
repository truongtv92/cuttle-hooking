using System;
using UnityEngine;

[Serializable]
public class Score
{
    [SerializeField]private float point;
    [SerializeField]private float timeValue;


    public float Point
    {
        get => point;
        set => point = value;
    }

    public float TimeValue
    {
        get => timeValue;
        set => timeValue = value;
    }
}

public enum ScoreType
{
    Point
}