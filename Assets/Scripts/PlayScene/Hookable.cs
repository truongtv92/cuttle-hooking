using Common;
using Spine.Unity;
using UnityEngine;

public class Hookable : MonoBehaviour
{
    public float speed;
    [SerializeField]
    private Score score;

    public bool IsHooked { get; private set; }

    private void FixedUpdate()
    {
        MoveFollowTarget(FishingHook.Instance.transform);
    }

    private void MoveFollowTarget(Transform target)
    {
        if (!IsHooked) return;
        var transform1 = target.parent.transform;
        var rotation = transform1.rotation;
        Quaternion.Euler(rotation.x, 
            rotation.y, 
            90 + rotation.z);
        var position = target.position;
        var transform2= transform;
        transform2.position = new Vector3(position.x, 
            position.y - gameObject.GetComponent<Collider2D>().GetComponent<Collider2D>().bounds.size.y / 2, 
            transform.position.z);
        if (FishingLine.Instance.typeAction != TypeAction.Idle) return;
        GamePlay.Instance.IncreaseScore(score);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.GetComponent<FishingHook>() == null) return;
        if(FishingLine.Instance.typeAction!= TypeAction.Drop) return;
        SoundManager.Instance.PlaySquidHookRightSound();
        IsHooked = true;
       
        FishingLine.Instance.typeAction = TypeAction.Pull;
        FishingHook.Instance.StartPull(speed);
        var anim = GetComponent<SkeletonAnimation>();
        if(anim==null) return;
        if (anim.AnimationName == "Squid 1 Gold Move")
            anim.AnimationName = "Squid 1 Gold Idle";
        else if (anim.AnimationName == "Squid 1 Purple Move")
            anim.AnimationName = "Squid 1 Purple Idle";
        else if (anim.AnimationName == "Squid 1 Pink Move")
            anim.AnimationName = "Squid 1 Pink Idle";
        else if (anim.AnimationName == "Squid 2 Gold Move")
            anim.AnimationName = "Squid 2 Gold Idle";
        else if (anim.AnimationName == "Squid 2 Purple Move")
            anim.AnimationName = "Squid 2 Purple Idle";
        else if (anim.AnimationName == "Squid 2 Pink Move")
            anim.AnimationName = "Squid 2 Pink Idle";
        else if (anim.AnimationName == "Squid 3 Gold Move")
            anim.AnimationName = "Squid 3 Gold Idle";
        else if (anim.AnimationName == "Squid 3 Pink Move")
            anim.AnimationName = "Squid 3 Pink Idle";
        else if (anim.AnimationName == "Squid 3 Purple Move") anim.AnimationName = "Squid 3 Purple Idle";

        anim.loop = false;
    }
}