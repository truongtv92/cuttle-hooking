using UnityEngine;

public class Moveable : MonoBehaviour
{
    [SerializeField]
    private Vector3 speed;
    public Vector3 Speed
    {
        get => speed;
        set => speed = value;
    }

    private void Update()
    {
        if (GamePlay.Instance.gameState ==GameState.Pause) return;
        if (!GetComponent<Hookable>().IsHooked)
        {
            Move();
        }

    }

    private void Move()
    {
        transform.position += speed * Time.deltaTime;
        if(Mathf.Abs(transform.position.x)>=9.7f) Destroy(gameObject);
    }
}