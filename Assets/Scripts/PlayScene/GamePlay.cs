using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Common;
using PlayScene;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GamePlay : Singleton<GamePlay>
{
    [SerializeField] private Timer timer;
    public Slider process;
    public Sprite[] PlayerSprite;
    public Sprite[] PlayerEmotionSprites;
    public TextMeshProUGUI TxtCountdown;
    public TextMeshProUGUI TxtScoreIncrease;
    public TextMeshProUGUI TxtEmotion;
    public Image PlayerImage;
    public Image PlayerEmotionImage;
    public GameObject[] goldSquids;
    public GameObject[] purpleSquids;
    public GameObject[] pinkSquids;
    public GameObject fish;
    public Transform HookableParent;
    public EndGamePopUp endGamePopUp;
    public float duration = 0.5f;
    public GameState gameState = GameState.CountDown;
    public TextMeshProUGUI scoreText;
    private int _timeCountDown = 3;
    public GameObject hooking;
    private GameObject popup;
    public GameObject lastReward;
    private readonly List<GameObject[]> _listSquids = new List<GameObject[]>();

    public float ScorePoint { get; private set; }

    #region Unity Method

    private void Awake()
    {
        SoundManager.Instance.StopBackgroundMusic();
        SoundManager.Instance.PlayInGameBackgroundMusic();
    }

    private void Start()
    {
        PlayerImage.sprite = PlayerSprite[Config.characterIndex];
        _listSquids.Clear();
        _listSquids.Add(goldSquids);
        _listSquids.Add(purpleSquids);
        _listSquids.Add(pinkSquids);
        hooking.SetActive(false);
        scoreText.text = "Total: " + ScorePoint + " kg";
        gameState = GameState.CountDown;
        InvokeRepeating(nameof(InitMuc), 0f, 1.25f);
        InvokeRepeating(nameof(InitFish),0f,2.5f);
        InvokeRepeating(nameof(ShowEmotionText),0,0.5f);
    }


    #endregion

    #region Public Method

    public void TouchScene()
    {
        FishingHook.Instance.TouchScene();
    }

    public void StartCountDown()
    {
        hooking.SetActive(true);
        InvokeRepeating(nameof(CountDownForBattle), 1f, 1f);
    }

    public void ShowEmotion(bool isHook)
    {
        PlayerEmotionImage.gameObject.SetActive(true);
        PlayerEmotionImage.sprite = isHook ? PlayerEmotionSprites[Config.characterIndex * 2] : PlayerEmotionSprites[Config.characterIndex * 2+1];
        if(!isHook)SoundManager.Instance.PlayMissSound();
        else SoundManager.Instance.PlayHookrightSound();
        Invoke(nameof(TurnOffEmotion),1f);
    }
    public void OnPauseButtonClick()
    {
        switch (gameState)
        {
            case GameState.Play:
                gameState = GameState.Pause;
                break;
            case GameState.Pause:
                gameState = GameState.Play;
                break;
            case GameState.Finish:
                break;
            case GameState.CountDown:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void EndGame()
    {
        gameState = GameState.Finish;
        CancelInvoke();
        foreach (var moveable in HookableParent.GetComponentsInChildren<Moveable>())
        {
            moveable.Speed *= 5f;
        }

        if (ScorePoint > PlayerPrefs.GetFloat("1st_value"))
        {
            PlayerPrefs.SetFloat("1st_value",ScorePoint);
            PlayerPrefs.SetString("1st_time",DateTime.UtcNow.ToString());
            PlayerPrefs.Save();
        }
        else if (ScorePoint > PlayerPrefs.GetFloat("2nd_value"))
        {
            PlayerPrefs.SetFloat("2nd_value",ScorePoint);
            PlayerPrefs.SetString("2nd_time",DateTime.UtcNow.ToString());
            PlayerPrefs.Save();
        }
        else if (ScorePoint > PlayerPrefs.GetFloat("3rd_value"))
        {
            
            PlayerPrefs.SetFloat("3rd_value",ScorePoint);
            PlayerPrefs.SetString("3rd_time",DateTime.UtcNow.ToString());
            PlayerPrefs.Save();
        }
        Invoke(nameof(ShowEndgamePopUp), 1.5f);
    }

    #endregion

    void TurnOffEmotion()
    {
        PlayerEmotionImage.gameObject.SetActive(false);
    }
    private void ShowEndgamePopUp()
    {
        endGamePopUp.gameObject.SetActive(true);
        endGamePopUp.Init(ScorePoint);
    }

    private void CountDownForBattle()
    {
        TxtCountdown.text = _timeCountDown > 0 ? "" + _timeCountDown : "Go!";
        _timeCountDown--;
        if (_timeCountDown >= -1) return;
        CancelInvoke(nameof(CountDownForBattle));
        StartGame();
    }

    private void StartGame()
    {
        gameState = GameState.Play;
        TxtCountdown.text = "";
        timer.StartCountDown();
        

    }


    public void IncreaseScore(Score score)
    {
        var lastScore = ScorePoint;
        ScorePoint += score.Point;
        var current = ScorePoint;
        timer.AddMoreTime(score.TimeValue);
        // StopCoroutine("UpdateScore");
        StartCoroutine(UpdateScore(lastScore, current));
        
        //scoreText.text = "Score: "+scorePoint;
        ShowEmotion(score.TimeValue>0);

    }

    private IEnumerator UpdateScore(float last, float current)
    {
        for (float timeCount = 0; timeCount < duration; timeCount += Time.deltaTime)
        {
            var progress = timeCount / duration;
            var data = Mathf.Lerp(last, current, progress);
            scoreText.text = "Total: " + data.ToString("0.00") + " kg";
            process.value =Config.CalculateProcess(data); 
            yield return null;
        }

        scoreText.text = "Total: " + current.ToString("0.00") + " kg";
        process.value = Config.CalculateProcess(current); 

        yield return null;
    }

    private void InitMuc()
    {
        for (var i = 0; i < 1; i++)
        {
            var randomType = Random.Range(0, 100);
            if (randomType < 10) randomType = 0;
            else if (randomType < 30) randomType = 1;
            else randomType = 2;
            var randomValue = Random.Range(0, 3);

            var muc = Instantiate(_listSquids[randomType][randomValue]);
            muc.transform.parent = HookableParent;
            var isMoveFromLeft = Random.Range(0, 1f) < 0.5f;
            if (!isMoveFromLeft)
            {
                muc.transform.position = new Vector3(9.19f, Random.Range(-3.8f, -0.6f));
                muc.GetComponent<Moveable>().Speed *= -1;
                muc.transform.localScale = new Vector3(muc.transform.localScale.x * -1, muc.transform.localScale.y,
                    muc.transform.localScale.z);
            }
            else
            {
                muc.transform.position = new Vector3(-9.19f, Random.Range(-3.8f, -0.6f));
            }
        }

    }

    private void InitFish()
    {
        for (var i = 0; i < 1; i++)
        {
            var muc = Instantiate(fish);
            muc.transform.parent = HookableParent;
            muc.GetComponent<Moveable>().Speed = new Vector3(Random.Range(1, 5f),0,0);
            var isMoveFromLeft = Random.Range(0, 1f) < 0.5f;
            if (!isMoveFromLeft)
            {
                muc.transform.position = new Vector3(9.19f, Random.Range(-2f, -0.6f));
                muc.GetComponent<Moveable>().Speed *= -1;
                muc.transform.localScale = new Vector3(muc.transform.localScale.x * -1, muc.transform.localScale.y,
                    muc.transform.localScale.z);
            }
            else
            {
                muc.transform.position = new Vector3(-9.19f, Random.Range(-2f, -0.6f));
            }
            
        }
    }
    public void TakeScreenShot()
    {
        StartCoroutine(captureScreenshot());
    }
    IEnumerator captureScreenshot()
    {
        yield return new WaitForEndOfFrame();
        var screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();
        var imageBytes = screenImage.EncodeToPNG();
        NativeGallery.SaveImageToGallery(imageBytes, "Squid Hunting", "SquidHunting_{0}.png");
        //System.IO.File.WriteAllBytes(path, imageBytes);
        lastReward.SetActive(true);
        
    }

    void ShowEmotionText()
    {
        if (TxtEmotion.text.Equals(".")) TxtEmotion.text = "..";
        else if (TxtEmotion.text.Equals("..")) TxtEmotion.text = "...";
        else if (TxtEmotion.text.Equals("...")) TxtEmotion.text = "";
        else if(TxtEmotion.text.Equals(""))TxtEmotion.text = ".";
    }

}

public enum GameState
{
    Play,
    Pause,
    Finish,
    CountDown
}